CREATE TABLE car (
id INTEGER PRIMARY KEY AUTOINCREMENT, 
mark varchar(100) NOT NULL, 
model varchar(100) NOT NULL,
equipment varchar(100) NOT NULL,
power int(5) NOT NULL,
image varchar(255),  
color varchar(10) NOT NULL,  
price int(11) NOT NULL,
status varchar(10) NOT NULL);
INSERT INTO car (mark, model, equipment, power, image, color, price, status) VALUES ('BMW', 'X5', 'First', 500, 'image1.jpg', 'red', 1000000, 'free');
INSERT INTO car (mark, model, equipment, power, image, color, price, status) VALUES ('Mercedes', 'GLE', '03B', 800, 'image2.jpg', 'blue', 1500000, 'free');
INSERT INTO car (mark, model, equipment, power, image, color, price, status) VALUES ('Audi', 'Q7', 'TR7', 400, 'image3.jpg', 'green', 2000000, 'free');
INSERT INTO car (mark, model, equipment, power, image, color, price, status) VALUES ('BMW', 'X6', 'Second', 650, 'image4.jpg', 'yellow', 1400000, 'free');
INSERT INTO car (mark, model, equipment, power, image, color, price, status) VALUES ('Honda', 'Civic', '2DD', 200, 'image5.jpg', 'gray', 700000, 'free');
