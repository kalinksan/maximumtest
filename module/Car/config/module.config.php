<?php

namespace Car;

use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    /*'controllers' => [
        'factories' => [
            Controller\CarController::class => InvokableFactory::class,
        ],
    ],*/

    // The following section is new and should be added to your file:
    'router' => [
        'routes' => [
            'car' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/car[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\CarController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'api' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api[/:id]',
                    'constraints' => [
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ApiController::class,
                    ],
                ],
            ],
        ],
    ],
/*
    'controllers' => array(
        'invokables' => array(
            'Car\Controller\Car' => 'Car\Controller\CarController',
            'Car\Controller\Api' => 'Car\Controller\ApiController',
        ),
    ),*/
    
    'view_manager' => [
        'template_path_stack' => [
            'car' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];