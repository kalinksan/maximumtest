<?php

namespace Car\Controller;

use Car\Model\CarTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CarController extends AbstractActionController {

    private $table;

    public function __construct(CarTable $table) {
        $this->table = $table;
    }

    public function indexAction() {
        return new ViewModel([
            'cars' => $this->table->fetchAll(),
        ]);
    }
}
