<?php

namespace Car\Controller;
use Car\Model\CarTable;
use Car\Controller\AbstractRestfulJsonController;
use Zend\View\Model\JsonModel;

class ApiController extends AbstractRestfulJsonController {

    private $table;

    public function __construct(CarTable $table) {
        $this->table = $table;
    }

    public function update($id, $data) {
        $this->table->updateStatus($id, $data);
        return new JsonModel(["OK"]);
    }

}
