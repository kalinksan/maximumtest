<?php

namespace Car\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGateway;

class CarTable {

    private $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll() {
        return $this->tableGateway->select();
    }

    public function getCar($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function updateStatus($id, $data) {
        $id = (int) $id;
        $status = $data['status'];
        if ($this->getCar($id) && isset(Car::$statusType[$status])) {
            $this->tableGateway->update(['status' => $status], ['id' => $id]);
        } else {
            throw new \Exception('Car id does not exist');
        }
    }

}
