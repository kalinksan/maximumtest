<?php

namespace Car\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Car implements InputFilterAwareInterface {

    const STATUS_FREE = 'free';
    const STATUS_BOOKED = 'booked';
    const STATUS_SOLD = 'sold';

    public static $statusType = [
        self::STATUS_FREE => 'Свободен',
        self::STATUS_BOOKED => 'Забронирован',
        self::STATUS_SOLD => 'Продан'
    ];

    public function exchangeArray(array $data) {
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->mark = !empty($data['mark']) ? $data['mark'] : null;
        $this->model = !empty($data['model']) ? $data['model'] : null;
        $this->power = !empty($data['power']) ? $data['power'] : null;
        $this->equipment = !empty($data['equipment']) ? $data['equipment'] : null;
        $this->color = !empty($data['color']) ? $data['color'] : null;
        $this->image = !empty($data['image']) ? $data['image'] : null;
        $this->price = !empty($data['price']) ? $data['price'] : null;
        $this->status = !empty($data['status']) ? $this->getStatus($data['status']) : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        
    }

    public function getInputFilter() {
        
    }

    public function getStatus($status) {
        return self::$statusType[$status];
    }

}
